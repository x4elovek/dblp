package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.*;
import mydblp.orm.QueryHandler;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/WEB-INF/jspf/searcher.jspf");
    _jspx_dependants.add("/WEB-INF/jspf/result.jspf");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>DBLP</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <h1>Welcome to DBLP database!</h1>\n");
      out.write("        ");
      out.write("\n");
      out.write("<form name=\"search\" action=\"index.jsp\">\n");
      out.write("    <input type=\"text\" name=\"query\" value=\"\" size=\"64\" />\n");
      out.write("    <select name=\"type\">\n");
      out.write("        <option value=\"Publication\">Publication</option>\n");
      out.write("        <option value=\"People\">People</option>\n");
      out.write("        <option value=\"Venue\">Venue</option>\n");
      out.write("    </select>\n");
      out.write("     <select name=\"sort\">\n");
      out.write("        <option value=\"Date\">Date</option>\n");
      out.write("        <option value=\"Name\">Name</option>\n");
      out.write("    </select>\n");
      out.write("</form>\n");
      out.write("\n");
      out.write("        ");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write(" \n");


    String query = request.getParameter("query");
    ResultSet persons = null;
    ResultSet venues = null;
    ResultSet publications = null;
    if (query != null && !query.trim().isEmpty()) {
        Class.forName("org.postgresql.Driver");
        Connection connection = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/DBLP", "justuser", "1");
        if (connection != null) {
            Statement st = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            String type = request.getParameter("type");
            if (type == null) {
                type = "publication";
            } else {
                type = type.toLowerCase();
            }
            if (type.equals("people")) {
                persons = QueryHandler.findPersons(query, st);
            } else if (type.equals("publication")) {
                publications = QueryHandler.findPublications(query, st);
            } else if (type.equals("publof")) {
                publications = QueryHandler.findPublicationsOfPerson(query, st);
            } else if (type.equals("fromproceedings")) {
                publications = QueryHandler.findPublicationsFrom(query, st);
            } else if (type.equals("venue")) {
                venues = QueryHandler.findVenues(query, st);
            } else {
                publications = QueryHandler.findPublications(query, st);
            }
            connection.close();
        }
    } else {
        query = "";
    }
      out.write('\n');
      out.write('\n');
if (persons != null) {
        out.println("<h1>Persons</h1>");
        while (persons.next()) {
            out.print("<a href=index.jsp?query=" + persons.getString("personid") + "&type=publof>" + persons.getString("name") + "</a></br>");
        }
    }
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");


    if (publications != null) {
        out.println("<h1>Publications</h1>");
        String publid = "";// publications.getString("publicationid");
        boolean firstBook = true;
        out.println("<ul type = \"disc\">");
        while (publications.next()) {
            if (!publications.getString("publicationid").equals(publid)) {
                if (firstBook) {
                    out.println("</li>");
                    firstBook = false;
                }
                out.println("<li>");
                out.println(QueryHandler.show(publications.getString("title")) + QueryHandler.show(publications.getString("booktitle")) + QueryHandler.show(publications.getString("date"))
                        + "<br/>");
                out.println(QueryHandler.show(publications.getString("journal")) + QueryHandler.show(publications.getString("number")) + QueryHandler.show(publications.getString("pages")));
                publid = publications.getString("publicationid");
            }
            out.print("<a href=index.jsp?query=" + publications.getString("personid") + "&type=publof>" + publications.getString("name") + "</a> ");

        }
        out.println("</ul>");
    }
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");


    if (venues != null) {
        out.println("<h1>Venues</h1>");
        while (venues.next()) {
            out.print("<a href=index.jsp?query=" + venues.getString("venue").replaceAll(" ", "%20") + "&type=fromproceedings>" + venues.getString("venue") + "</a> </br>");
        }
    }

      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
      out.write("    ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
