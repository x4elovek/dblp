package mydblp.orm;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QueryHandler {

    public static ResultSet findPersons(String query, Statement st) {
        try {
            query = query.toLowerCase();
            System.out.println("SELECT * FROM PERSONS WHERE name LIKE '" + query + "%' limit 15");
            return st.executeQuery("SELECT * FROM PERSONS WHERE name LIKE '" + query + "%' limit 15");
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static ResultSet findPublications(String query, String sort, Statement st) {
        String dbSubQuery = "";
        if (sort.equals("name")) {
            sort = "title";
        } else {
            sort = "date";
        }
        try {
            String keywords[] = query.toLowerCase().split(" ");
            int words = 0;
            for (int i = 0; i < keywords.length; i++) {
                if (keywords[i].length() > 5) {
                    if (words > 1) {
                        dbSubQuery += " using (publicationid) ";
                    }
                    if (words > 0) {
                        dbSubQuery += " inner join ";
                    }
                    dbSubQuery += "(SELECT publicationid FROM keywords WHERE keyword='" + keywords[i].replaceAll("[^a-z1-9-]", "") + "')t" + words;
                    words++;
                }
            }
            if (words > 1) {
                dbSubQuery += " using (publicationid) ";
            }
            if (words == 1) {
                dbSubQuery = "SELECT * FROM " + dbSubQuery;
            } else {
                dbSubQuery = "SELECT * FROM (" + dbSubQuery + ")";
            }
            dbSubQuery += "  limit 10 ";
            String dbQuery = "with pubs as (" + dbSubQuery + "), personsid as (select * from (SELECT pubs.publicationid,a.authorid as personid from author a natural join pubs)a1 union (SELECT pubs.publicationid,e.editorid as personid  from editor e natural join pubs)) select * from (select distinct on (personid,publicationid) * from personsid natural join names NATURAL JOIN PUBLICATION) as v order by (" + sort + ",publicationid) desc";
            System.out.println(dbQuery);
            return st.executeQuery(dbQuery);
        } catch (SQLException ex) {
            System.err.print("ошибка" + ex);
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static ResultSet findPublicationsOfPerson(String query, String sort, Statement st) {
        try {
            if (sort.equals("name")) {
                sort = "title";
            } else {
                sort = "date";
            }
            String dbquery = "SELECT distinct publicationid FROM author where authorid='" + query + "' union SELECT publicationid FROM editor where editorid='" + query + "'";
            dbquery = "with pubs as (" + dbquery + "), personsid as (select * from (SELECT pubs.publicationid,a.authorid as personid from author a natural join pubs)a1 union (SELECT pubs.publicationid,e.editorid as personid  from editor e natural join pubs)) select * from (select distinct on (personid,publicationid) * from personsid natural join names NATURAL JOIN PUBLICATION) as v order by (" + sort + ",publicationid) desc";
            System.out.println(dbquery);
            return st.executeQuery(dbquery);
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static ResultSet findPublicationsFrom(String query, String sort, Statement st) {
        try {
            if (sort.equals("name")) {
                sort = "title";
            } else {
                sort = "date";
            }
            String dbQuery = "with pubs as (Select distinct publicationid from publication where booktitle='" + query + "' or journal='" + query + "' ), personsid as (select * from (SELECT pubs.publicationid,a.authorid as personid from author a natural join pubs)a1 union (SELECT pubs.publicationid,e.editorid as personid  from editor e natural join pubs)) select * from (select distinct on (personid,publicationid) * from personsid natural join names NATURAL JOIN PUBLICATION) as v order by (" + sort + ",publicationid) desc";
            System.out.println(dbQuery);
            return st.executeQuery(dbQuery);
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static ResultSet findVenues(String query, Statement st) {
        try {
            String dbSubQuery = "with pubs as(select * from publication where type='proceedings') select distinct on (venue) booktitle as venue from pubs where booktitle like '" + query + "%' union select distinct on (venue) journal as venue from pubs where journal like '" + query + "%' order by(venue) asc";
            System.out.println(dbSubQuery);
            return st.executeQuery(dbSubQuery);
        } catch (SQLException ex) {
            Logger.getLogger(QueryHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static String show(String s) {
        if (s != null && s.trim().length() > 0) {
            return s + " ";
        } else {
            return "";
        }
    }

}
